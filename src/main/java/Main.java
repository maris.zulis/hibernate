import example.dao.PersonDao;
import example.entity.Dog;
import example.entity.Person;
import example.entity.PersonType;
import example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class Main {

    public static void main (String[] args){
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();

            //CREATE
            Dog dog = new Dog();
            Person person = new Person("Henry", "Haines", "HenrySHaines@dayrep.com", "United States", PersonType.LABS, dog, null, null);

            dog.setName("Duksis");
            dog.setBarks(true);

            //create person
            session.beginTransaction();
            session.save(dog);
            session.save(person);
            //session.save(dog);

            person.setEmail("aaaa@aaaa.lv");
            session.getTransaction().commit();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            //get person
            session.beginTransaction();
            dog = session.find(Dog.class, 1L);
            person = session.find(Person.class, 1L);
            session.getTransaction().commit();

            System.out.println(person.toString());
            System.out.println("---------------------------");

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            //UPDATE
            session.beginTransaction();
            person.setCountry("Canada");
            session.update(person);
            session.getTransaction().commit();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            person = session.find(Person.class, 1L);
            session.getTransaction().commit();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            System.out.println(person.toString());
            System.out.println("---------------------------");

            //DELETE
            session.beginTransaction();
            session.delete(person);
            session.getTransaction().commit();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            //SELECT
            session.beginTransaction();
            List<Person> persons = session.createQuery("from Person", Person.class).list();
            for (Person p : persons) {
                System.out.println(p.toString());
            }
            session.getTransaction().commit();

            Session session2 = HibernateUtil.getSessionFactory().getCurrentSession();
                session2.beginTransaction();
                session2.save(person);

                person.setEmail("aaa@aaa.lv");
                session2.update(person);
                person.setEmail("aaa2@aaa2.lv");
                session2.update(person);
                session2.getTransaction().rollback();
        } finally {
            HibernateUtil.shutdown();
        }



    }
}
